/* $OpenLDAP$ */
/* This work is part of OpenLDAP Software <http://www.openldap.org/>.
 *
 * Copyright 1998-2013 The OpenLDAP Foundation.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted only as authorized by the OpenLDAP
 * Public License.
 *
 * A copy of this license is available in the file LICENSE in the
 * top-level directory of the distribution or, alternatively, at
 * <http://www.OpenLDAP.org/license.html>.
 */
/* This file contains data common to samba4 overlays and access
   and helper functions */

#include "portable.h"
#include <stdio.h>

#include "ac/string.h"
#include "ac/socket.h"

#include "slap.h"
#include "config.h"

#include "lutil.h"
#include "ldap_rq.h"
#include <ldb.h>
#include <talloc.h>
#include "samba_security.h"
#include "core/werror.h"
#include "overlay_utils.h"
#include "samba_flags.h"
#include <ndr.h>
/* pointers to databases of the commonly used partitions */
static 	BackendDB	*schema_db = NULL;
static 	BackendDB	*config_db = NULL;
static 	BackendDB	*domain_db = NULL;


int set_partitions_db_pointers(BackendDB *be)
{
	/* find the configuration, schema and domain naming context db's.
	 * probably not the best way to do this, change when root_dse overlay is available */
	struct berval config_rdn;
	struct berval schema_rdn;
	struct berval domain_dn;
	struct berval rdn;
	config_rdn.bv_val = "cn=configuration";
	config_rdn.bv_len = strlen(config_rdn.bv_val);
	schema_rdn.bv_val = "cn=schema";
	schema_rdn.bv_len = strlen(schema_rdn.bv_val);
	dnRdn(be->be_nsuffix, &rdn);
	if (ber_bvcmp(&config_rdn, &rdn) == 0) {
		if (config_db == NULL) {
			config_db = select_backend(&be->be_nsuffix[0], 0);
		}
		if (domain_db == NULL) {
			dnParent(&be->be_nsuffix[0], &domain_dn);
			domain_db = select_backend(&domain_dn, 0);
		}
		return 0;
	}
	if (schema_db == NULL && ber_bvcmp(&schema_rdn, &rdn) == 0) {
		schema_db = select_backend(&be->be_nsuffix[0], 0);
	}
	return 0;
}

/* data access functions */

struct dsdb_schema* get_samba_schema()
{
	slap_overinst	*on;
	struct dsdb_schema *schema = NULL;
	schema4_info  *si = NULL;
	assert(schema_db != NULL);

	if (!overlay_is_over(schema_db)) {
		return NULL;
	}
	
	on = ((slap_overinfo *)schema_db->bd_info->bi_private)->oi_list;
	for ( ; on; on = on->on_next ) {
		if ( strcmp( on->on_bi.bi_type, "samba4_schema" ) == 0 ) {
			si = (schema4_info *)on->on_bi.bi_private;
			assert(si != NULL);
			schema = si->schema;
			break;
		}
	}

	return schema;	
}

BackendDB *get_schema_db()
{
	/* should not be used unless initialized */
	assert(schema_db != NULL);
	return schema_db;
}
BackendDB *get_domain_db()
{
	/* should not be used unless initialized */
	assert(domain_db != NULL);
	return domain_db;
}

BackendDB *get_config_db()
{
	/* should not be used unless initialized */
	assert(config_db != NULL);
	return config_db;
}

struct berval *samba_aggregate_schema_dn(void *memctx) 
{
	struct berval aggregate;
	struct berval *new_dn = (struct berval *)slap_sl_malloc(sizeof(struct berval), memctx );
	ber_str2bv("CN=Aggregate", STRLENOF( "CN=Aggregate" ), 1, &aggregate);
	build_new_dn(new_dn, &schema_db->be_nsuffix[0], &aggregate, memctx);
	return new_dn;
}

bool dn_has_extended(struct berval *dn)
{
	/* TODO do we need anything else? */
	if (dn != NULL && dn->bv_len != 0
	    && dn->bv_val[0] == '<') {
		return true;
	}
	return false;
}

int dn_remove_extended( struct berval *dn, struct berval *dst, void *ctx )
{
	struct berval tmp, out;
	
	if (dn == NULL || dn->bv_len == 0) {
		return LDAP_SUCCESS;
	}

	/* do we actually need to do something?*/
	if (!dn_has_extended(dn)) {
		return LDAP_SUCCESS;
	}

	/* do this to avoid freeing of the original dn
	 by the caller */
	if (!dn_has_extended(dn)) {
		tmp.bv_val = dn->bv_val;
		tmp.bv_len = dn->bv_len;
	}
	else {
		tmp.bv_val = strrchr(dn->bv_val, '>');
		if (tmp.bv_val == NULL) {
			/* log invalid dn, what to return */
			return LDAP_INVALID_DN_SYNTAX;
		}

		tmp.bv_val ++; /* do we need to skip a ,?*/
		tmp.bv_len = dn->bv_val - tmp.bv_val;
	}

	ber_dupbv_x(&out, &tmp, ctx);
	if (dst == NULL) {
		ch_free(dn->bv_val);
		assert (ber_dupbv_x(dn, &out, ctx) != NULL);
	}
	else {
		ber_dupbv_x(dst, &out, ctx);	
	}

	ch_free(out.bv_val);
	return LDAP_SUCCESS;
}

int ov_execute_search(Operation *op,
		      SlapReply *rs,
		      BackendDB *backend_db,
		      struct berval *base,
		      char **attr_list,
		      int attr_count,
		      char *filter,
		      int scope,
		      slap_callback *cb,
		      LDAPControl **ctrls,
		      bool skip_ol_stack)
{
	Operation new_op = *op;
	SlapReply new_rs = { 0 };
	AttributeDescription *descr = NULL;
	BackendDB db;
	slap_overinst	*on;
	int i;
	AttributeName *an = (AttributeName *)op->o_tmpalloc(sizeof(AttributeName)*(attr_count + 1),
									 op->o_tmpmemctx );
	for (i=0; i < attr_count; i++) {
		if ((descr = ov_find_attr_description(attr_list[i])) == NULL) {
			op->o_tmpfree( an, op->o_tmpmemctx );
			return rs->sr_err;
		} /* todo is this allright? */
		an[i].an_name.bv_len = strlen(attr_list[i]);
		an[i].an_name.bv_val = attr_list[i];
		an[i].an_desc = descr;
	}

	an[attr_count].an_desc = NULL;
	an[attr_count].an_name.bv_len = 0;
	an[attr_count].an_name.bv_val = NULL;
	if (backend_db == NULL) {
		db = *op->o_bd;
	}
	else {
		db = *backend_db;
	}

	on = (slap_overinst *)db.bd_info;
	new_op.o_tag = LDAP_REQ_SEARCH;
	new_op.o_callback = cb;
	new_op.o_req_dn = *base;
	dnNormalize( 0, NULL, NULL, &new_op.o_req_dn, &new_op.o_req_ndn, NULL );

      	new_op.o_bd = &db;
/*	if (skip_ol_stack) {
		new_op.o_bd->bd_info = on->on_info->oi_orig;
	}
	else {
		new_op.o_bd->bd_info = (BackendInfo *)on->on_info;
		} */
	new_op.o_dn = new_op.o_bd->be_rootdn;
	new_op.o_ndn = new_op.o_bd->be_rootndn;
	new_op.ors_limit = NULL;
	new_op.ors_slimit = 1;
	new_op.ors_tlimit = SLAP_NO_LIMIT;
	new_op.ors_attrs = an;
	new_op.ors_attrsonly = 1;
	new_op.ors_deref = LDAP_DEREF_NEVER;
	new_op.ors_scope = scope;
	if (ctrls != NULL) {
		int j;
		for ( j=0; ctrls[j]; j++ ) ;

		new_op.o_ctrls = op->o_tmpalloc(( j+1 )*sizeof(LDAPControl *), op->o_tmpmemctx );
		for ( j = 0; ctrls[j]; j++ ) {
			new_op.o_ctrls[i] = ctrls[i];
		}
	}
	
	new_op.ors_filterstr.bv_len = STRLENOF( filter );
	new_op.ors_filterstr.bv_val = filter;
	new_op.ors_filter = str2filter_x( op, new_op.ors_filterstr.bv_val );
	assert( new_op.ors_filter != NULL );
	(void)new_op.o_bd->be_search(&new_op, &new_rs);
	op->o_tmpfree( an, op->o_tmpmemctx );
	filter_free_x( op, new_op.ors_filter, 1 );

	return new_rs.sr_err;
}

AttributeDescription* ov_find_attr_description(const char *attr_name)
{
	int rc;
	const char *text = NULL;
	AttributeDescription *attr_description = NULL;
	rc = slap_str2ad( attr_name, &attr_description, &text );
	if ( rc != LDAP_SUCCESS ) {
			Debug( LDAP_DEBUG_ANY,
			"ov_find_attr_description: Failed to find description of %s (%d), %s\n",
			attr_name, rc, text );
		return NULL;
	}
	return attr_description;
}

Attribute* ov_find_attribute(Attribute *attr_list, const char *attr_name)
{
	AttributeDescription *attr_descr = NULL;
	attr_descr = ov_find_attr_description(attr_name);
	if (attr_descr == NULL) {
		return NULL;
	}
	return attr_find(attr_list, attr_descr);
}


Modification* ov_find_modification(Modifications *mod_list, const char *attr_name)
{
	AttributeDescription *attr_descr = NULL;
	Modifications *ml;
	attr_descr = ov_find_attr_description(attr_name);
	if (attr_descr == NULL) {
		return NULL;
	}
	for ( ml = mod_list; ml != NULL; ml = ml->sml_next ) {
		if ( ml->sml_mod.sm_desc == attr_descr ) {
			return &ml->sml_mod;
		}
	}
	return NULL;
}


/*ldb_msg_find_attr_as_int(*/
int ov_find_attribute_int(Attribute *attr_list, const char *attr_name, int default_val, int index)
{
	Attribute *attr = ov_find_attribute(attr_list, attr_name);
	if (attr == NULL || attr->a_numvals < index+1) {
		return default_val;
	}
	return (int)strtol(attr->a_vals[index].bv_val,0,0);
}

/*
* char *ol_op_timestring(Operation *op, time_t t) 
* similar to (and based on) ldb_timestring, returns a LDAP formatted generalized time string
* op is used for the memory context it holds and the t_alloc callback
*/

int ov_timestring(struct berval *val, time_t t)
{
	struct tm *tm = gmtime(&t);
	char ts[18];
	int r;
	if (!tm) {
		return LDAP_OPERATIONS_ERROR;
	}
	
	/* formatted like: 20040408072012.0Z */
	r = snprintf(ts, 18,
		     "%04u%02u%02u%02u%02u%02u.0Z",
		     tm->tm_year+1900, tm->tm_mon+1,
		     tm->tm_mday, tm->tm_hour, tm->tm_min,
			tm->tm_sec);
	
	if (r != 17) {
		return LDAP_OPERATIONS_ERROR;
	}

	*val = *(ber_bvstr(ts));
	return LDAP_SUCCESS;
}

/* callbacks for helper functions */
static int
get_domainsid_cb( Operation *op, SlapReply *rs )
{
	if ( rs->sr_type == REP_SEARCH ) {
		struct berval **d_sid = (struct berval **)op->o_callback->sc_private;
	       	AttributeDescription *obsid_descr = NULL;
		Attribute *objectsid_attribute = NULL;

		if ((obsid_descr = ov_find_attr_description("objectSid")) == NULL) {
			return rs->sr_err;
		}
		
		objectsid_attribute = attr_find( rs->sr_entry->e_attrs, obsid_descr);
		if ( objectsid_attribute == NULL) {
			*d_sid = NULL; 
			send_ldap_error( op, rs, LDAP_OPERATIONS_ERROR,
					"Could not find the domain SID" );
			return rs->sr_err;
		}
		if (objectsid_attribute->a_numvals !=1) {
			send_ldap_error( op, rs, LDAP_OPERATIONS_ERROR,
					 "Incorrect read of attribute objectSid" );
			return rs->sr_err;
		}
		*d_sid = &(objectsid_attribute->a_vals[0]);
	}

	return 0;
}

static int
secdescriptor_get_parent_cb( Operation *op, SlapReply *rs )
{
	/* todo cache nTDescriptorDescription, no point getting it every time */
	if ( rs->sr_type == REP_SEARCH ) {
		struct berval **p_sd = (struct berval **)op->o_callback->sc_private;
	       	AttributeDescription *secdesc_descr = NULL;
		Attribute *secdesc_attribute = NULL;
	
		if ((secdesc_descr = ov_find_attr_description("nTSecurityDescriptor")) == NULL) {
			return rs->sr_err;
		}
	       
		secdesc_attribute = attr_find( rs->sr_entry->e_attrs, secdesc_descr);
		if ( secdesc_attribute == NULL) {
			*p_sd = NULL; 
			return SLAP_CB_CONTINUE;
		}
		if (secdesc_attribute->a_numvals !=1) {
			send_ldap_error( op, rs, LDAP_OPERATIONS_ERROR,
					 "Incorrect read of attribute nTSecurityDescriptor" );
			return rs->sr_err;
		}
		*p_sd = &(secdesc_attribute->a_vals[0]);
	}

	return 0;
}

int get_parent_sd (Operation *op,
		   SlapReply *rs,
		   struct berval *instanceType,
		   DATA_BLOB **parent_sd)
{
	BackendDB db = *op->o_bd;
	Operation new_op = *op;
	SlapReply new_rs = { 0 };
	slap_callback cb = { 0 };
	slap_overinst *on = (slap_overinst *)op->o_bd->bd_info;
	AttributeName an[2];
	AttributeDescription *secdesc_descr = NULL;
	char *descr_name = "nTSecurityDescriptor";
	struct berval *p_sd = NULL;
	const char *filter = "(&)";
	
	assert(parent_sd != NULL);

	BER_BVZERO( &an[1].an_name );
	int flags_val = (int)strtol(instanceType->bv_val,0,0);
	if (flags_val & INSTANCE_TYPE_IS_NC_HEAD) {
		/* this is a naming context, it has no parent */
		return LDAP_SUCCESS;
	}
	
	if ((secdesc_descr = ov_find_attr_description("nTSecurityDescriptor")) == NULL) {
			return rs->sr_err;
	}

	an[0].an_name.bv_len = strlen(descr_name);
	an[0].an_name.bv_val = descr_name;
	an[0].an_desc = secdesc_descr;
	new_op.o_bd = &db;
	new_op.o_bd->bd_info = (BackendInfo *)on->on_info;
	new_op.o_tag = LDAP_REQ_SEARCH;
	new_op.o_dn = op->o_bd->be_rootdn;
	new_op.o_ndn = op->o_bd->be_rootndn;
	new_op.o_callback = &cb;
	cb.sc_response = secdescriptor_get_parent_cb;
	cb.sc_private = (void *)&p_sd;

	dnParent( &op->o_req_dn, &new_op.o_req_dn );
	new_op.o_req_ndn = new_op.o_req_dn;
	new_op.ors_limit = NULL;
	new_op.ors_slimit = 1;
	new_op.ors_tlimit = SLAP_NO_LIMIT;
	new_op.ors_attrs = an;
	new_op.ors_attrsonly = 1;
	new_op.ors_deref = LDAP_DEREF_NEVER;
	new_op.ors_scope = LDAP_SCOPE_BASE;
	new_op.ors_filterstr.bv_len = STRLENOF( filter );
	new_op.ors_filterstr.bv_val = op->o_tmpalloc( new_op.ors_filterstr.bv_len+1, op->o_tmpmemctx );
	memcpy((void *)new_op.ors_filterstr.bv_val, (void *)filter ,new_op.ors_filterstr.bv_len);
	new_op.ors_filterstr.bv_val[new_op.ors_filterstr.bv_len] = '\0';
	new_op.ors_filter = str2filter_x( op, new_op.ors_filterstr.bv_val );
	assert( new_op.ors_filter != NULL );
	(void)new_op.o_bd->be_search( &new_op, &new_rs );
	
	if (p_sd != NULL) {
		(*parent_sd) = (DATA_BLOB *)op->o_tmpalloc(sizeof(DATA_BLOB),
								      op->o_tmpmemctx );
		(*parent_sd)->data = op->o_tmpalloc(p_sd->bv_len,
						   op->o_tmpmemctx );
		(*parent_sd)->length = p_sd->bv_len;
		memcpy((void *)(*parent_sd)->data, (void *)(p_sd->bv_val),
		       p_sd->bv_len);
	}
	filter_free_x( op, new_op.ors_filter, 1 );
	op->o_tmpfree( new_op.ors_filterstr.bv_val, op->o_tmpmemctx );
	return new_rs.sr_err;
}


int get_domain_sid (Operation *op, SlapReply *rs, DATA_BLOB **domain_sid)
{
	Operation new_op = *op;
	SlapReply new_rs = { 0 };
	slap_callback cb = { 0 };
	AttributeName an[2];
	BER_BVZERO( &an[1].an_name );
	AttributeDescription *objectsid_descr = NULL;
	char *descr_name = "objectSid";
	struct berval *d_sid = NULL;
	const char filter[] = "(&)";
    
	if ((objectsid_descr = ov_find_attr_description("objectSid")) == NULL) {
			return rs->sr_err;
		}
	
	an[0].an_name.bv_len = strlen(descr_name);
	an[0].an_name.bv_val = descr_name;
	an[0].an_desc = objectsid_descr;
	
	new_op.o_tag = LDAP_REQ_SEARCH;
	new_op.o_callback = &cb;
	cb.sc_response = get_domainsid_cb;
	cb.sc_private = (void *)&d_sid;
	new_op.o_req_dn = domain_db->be_nsuffix[0];
	new_op.o_req_ndn = new_op.o_req_dn;
	new_op.o_bd = domain_db;
	new_op.o_dn = new_op.o_bd->be_rootdn;
	new_op.o_ndn = new_op.o_bd->be_rootndn;
	new_op.ors_limit = NULL;
	new_op.ors_slimit = 1;
	new_op.ors_tlimit = SLAP_NO_LIMIT;
	new_op.ors_attrs = an;
	new_op.ors_attrsonly = 1;
	new_op.ors_deref = LDAP_DEREF_NEVER;
	new_op.ors_scope = LDAP_SCOPE_BASE;
	new_op.ors_filterstr.bv_len = STRLENOF( filter );
	new_op.ors_filterstr.bv_val = op->o_tmpalloc( new_op.ors_filterstr.bv_len+1, op->o_tmpmemctx );
	memcpy((void *)new_op.ors_filterstr.bv_val, (void *)filter ,new_op.ors_filterstr.bv_len);
	new_op.ors_filterstr.bv_val[new_op.ors_filterstr.bv_len] = '\0';
	new_op.ors_filter = str2filter_x( op, new_op.ors_filterstr.bv_val );
	assert( new_op.ors_filter != NULL );
	(void)new_op.o_bd->be_search(&new_op, &new_rs);
	
	if (d_sid != NULL) {
		*domain_sid = (DATA_BLOB *)op->o_tmpalloc(sizeof(DATA_BLOB),
								      op->o_tmpmemctx );
		(*domain_sid)->data = op->o_tmpalloc(d_sid->bv_len,
						   op->o_tmpmemctx );
		(*domain_sid)->length = d_sid->bv_len;
		memcpy((void *)(*domain_sid)->data, (void *)(d_sid->bv_val),
		       d_sid->bv_len);
	}
	filter_free_x( op, new_op.ors_filter, 1 );
	op->o_tmpfree( new_op.ors_filterstr.bv_val, op->o_tmpmemctx );
	
	return new_rs.sr_err;
}

int ov_add_guid_val( Entry *e, char *name, struct GUID *guid)
{
	struct ldb_val v;
	NTSTATUS status;
	TALLOC_CTX *tmp_ctx;
	struct berval guid_val = {0, NULL};
	AttributeDescription *descr = ov_find_attr_description(name);
	if (descr == NULL) {
		return LDAP_NO_SUCH_ATTRIBUTE;
	}
	tmp_ctx =  talloc_init(NULL);

	status = GUID_to_ndr_blob(guid, tmp_ctx, &v);
	if (!NT_STATUS_IS_OK(status)) {
		talloc_free(tmp_ctx);
		return LDAP_OPERATIONS_ERROR;
	}
	guid_val.bv_len = v.length;
	guid_val.bv_val = (char *)v.data;
	/* does not seem like we have to copy as attr_merge_one does a copy */
	attr_merge_one(e, descr, &guid_val, NULL);
	talloc_free(tmp_ctx);
	return LDAP_SUCCESS;
}

int ov_add_uint64_val(Entry *e, char *name, uint64_t value)
{
	return ov_add_int64_val(e, name, (int64_t) value);
}

int ov_add_int64_val(Entry *e, char *name, int64_t value)
{
	char val_buf[ LDAP_PVT_INTTYPE_CHARS( long long ) ];
	int val_len = sprintf(val_buf, "%lld", (long long) value);
	struct berval *val = NULL;
	AttributeDescription *descr = ov_find_attr_description(name);
	if (descr == NULL) {
		return LDAP_NO_SUCH_ATTRIBUTE;
	}
	if (val_len <= 0) {
		return LDAP_OPERATIONS_ERROR;
	}
	val = ber_bvstr(val_buf);
	attr_merge_one(e, descr, val, NULL);
	return LDAP_SUCCESS;
}


int ov_add_time_val(Entry *e, char *name, time_t value)
{
	struct berval val;
	int rc;
	AttributeDescription *descr = ov_find_attr_description(name);
	if (descr == NULL) {
		return LDAP_NO_SUCH_ATTRIBUTE;
	}
	if ((rc = ov_timestring(&val, value)) != LDAP_SUCCESS) {
		return rc;
	}

	attr_merge_one(e, descr, &val, NULL);
	return LDAP_SUCCESS;
}

/* This is a local samba connection */
bool ov_is_trusted_connection(Operation *op)
{
	struct berval sl_name;
	sl_name.bv_val = "PATH=/usr/local/samba/private/ldap/ldapi"; /*todo make this configurable!*/
	sl_name.bv_len = strlen(sl_name.bv_val);
	return (ber_bvcmp(&sl_name, &op->o_hdr->oh_conn->c_listener->sl_name) == 0);
}



/* Attribute to message element */
/* find the Opprepdata from op */
/* get last structural class by name */
