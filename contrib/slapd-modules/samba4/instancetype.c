/* $OpenLDAP$ */
/* This work is part of OpenLDAP Software <http://www.openldap.org/>.
 *
 * Copyright 1998-2013 The OpenLDAP Foundation.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted only as authorized by the OpenLDAP
 * Public License.
 *
 * A copy of this license is available in the file LICENSE in the
 * top-level directory of the distribution or, alternatively, at
 * <http://www.OpenLDAP.org/license.html>.
 */

/* This is a simple overlay to make sure all newly created objects
   have the correct instanceType */

#include "portable.h"

#ifdef SLAPD_OVER_INSTANCETYPE

#include <stdio.h>

#include "ac/string.h"
#include "ac/socket.h"

#include "slap.h"
#include "config.h"

#include "lutil.h"
#include "ldap_rq.h"
#include "samba_flags.h"

static slap_overinst 		instance_type;


static int instancetype_op_add( Operation *op, SlapReply *rs )
{
	Attribute *instance_attribute = NULL;
	AttributeDescription *instancetype_descr = NULL;
	int rc;
	const char *text = NULL;
	Debug(LDAP_DEBUG_ANY, "instancetype_op_add\n",0,0,0);
	rc = slap_str2ad( "instanceType", &instancetype_descr, &text );
	if ( rc != LDAP_SUCCESS ) {
		Debug( LDAP_DEBUG_ANY, "instancetype: unable to find attribute 'instanceType' (%d: %s, %d)\n",
		       rc, text, 0);
		send_ldap_error( op, rs, LDAP_NO_SUCH_ATTRIBUTE,
					"unable to find attribute 'instanceType'" );
			return rs->sr_err;
	}
	instance_attribute = attr_find( op->ora_e->e_attrs, instancetype_descr);
	if ( instance_attribute == NULL ) {
		int instance_flags = INSTANCE_TYPE_WRITE;
		char itflags_buf[ LDAP_PVT_INTTYPE_CHARS( unsigned long ) ];
		int itflags_len = sprintf(itflags_buf, "%0X", instance_flags);
		struct berval it_val;

		if (itflags_len <= 0) {
			send_ldap_error( op, rs, LDAP_OPERATIONS_ERROR,
					"Error creating instanceType." );
			return rs->sr_err;
		}
		it_val.bv_len = itflags_len;
		it_val.bv_val = ber_bvstr(itflags_buf);
		
		attr_merge_one(op->ora_e, instancetype_descr, &it_val, NULL);
		return SLAP_CB_CONTINUE;

	}
	else {
		if (instance_attribute->a_numvals != 1) {
			send_ldap_error( op, rs, LDAP_UNWILLING_TO_PERFORM,
					"instanceType is a single-valued attribute." );
			return rs->sr_err;
		}
		int flags_val = (int)strtol(instance_attribute->a_vals[0].bv_val,0,0);
		if (flags_val & INSTANCE_TYPE_IS_NC_HEAD) {
			if (!(flags_val & INSTANCE_TYPE_WRITE)) {
				send_ldap_error( op, rs, LDAP_UNWILLING_TO_PERFORM,
					"NC_HEAD is only compatible with WRITE" );
				return rs->sr_err;
			}
		}
		// only 0 and INSTANCE_TYPE_WRITE allowed
		else if ((flags_val !=0) && (flags_val != INSTANCE_TYPE_WRITE)) {
			send_ldap_error( op, rs, LDAP_UNWILLING_TO_PERFORM,
					"NC_HEAD is only compatible with WRITE" );
			return rs->sr_err;
		}
		return SLAP_CB_CONTINUE;
	}

	return SLAP_CB_CONTINUE;
}
static int
instancetype_op_modify( Operation *op, SlapReply *rs ) {
	AttributeDescription *instancetype_descr = NULL;
	int rc;
	const char *text = NULL;
	Modifications *ml;

	Debug(LDAP_DEBUG_ANY, "instancetype_op_mod\n",0,0,0);
	rc = slap_str2ad( "instanceType", &instancetype_descr, &text );
	if ( rc != LDAP_SUCCESS ) {
		Debug( LDAP_DEBUG_ANY, "instancetype: unable to find attribute 'instanceType' (%d: %s, %d)\n",
		       rc, text, 0);
		send_ldap_error( op, rs, LDAP_NO_SUCH_ATTRIBUTE,
					"instancetype: unable to find attribute 'instanceType'" );
			return rs->sr_err;
	}
	for ( ml = op->orm_modlist; ml != NULL; ml = ml->sml_next ) {
		if ( ml->sml_mod.sm_desc == instancetype_descr ) {
			send_ldap_error( op, rs, LDAP_CONSTRAINT_VIOLATION,
					 "instancetype: attribute is read-only" );
			return rs->sr_err;
		}
	}
	return SLAP_CB_CONTINUE;
}

/*TODO - we need to implement DBCHECK_CONTROL - allow modification if provided
 * it will not work at this point anyway unless we move sanity checks in a module below
 * that also supports the control */
/* TODO what to do when we need to distinguish between originating updates and replication
 * there may be a difference in behavior */

int instancetype_initialize(void)
{
	instance_type.on_bi.bi_type = "instancetype";
	instance_type.on_bi.bi_op_add = instancetype_op_add;
	instance_type.on_bi.bi_op_modify = instancetype_op_modify;
	Debug(LDAP_DEBUG_TRACE, "instancetype_initialize\n",0,0,0);
	return overlay_register(&instance_type);
}


#if SLAPD_OVER_INSTANCETYPE == SLAPD_MOD_DYNAMIC
int init_module( int argc, char *argv[] )
{
	return instancetype_initialize();
}
#endif /* SLAPD_OVER_INSTANCETYPE == SLAPD_MOD_DYNAMIC */

#endif /*SLAPD_OVER_INSTANCETYPE*/
