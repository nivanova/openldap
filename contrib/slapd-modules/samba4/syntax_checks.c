/* $OpenLDAP$ */
/* This work is part of OpenLDAP Software <http://www.openldap.org/>.
 *
 * Copyright 1998-2013 The OpenLDAP Foundation.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted only as authorized by the OpenLDAP
 * Public License.
 *
 * A copy of this license is available in the file LICENSE in the
 * top-level directory of the distribution or, alternatively, at
 * <http://www.OpenLDAP.org/license.html>.
 */

/* This is a simple overlay to perform the syntax checks usually
   performed before the request is passed to the frontend.
   This is necessary so the overlays above could handle errors and
   return required non-standard error codes */

#include "portable.h"

#ifdef SLAPD_OVER_SYNTAXCHECKS

#include <stdio.h>

#include "ac/string.h"
#include "ac/socket.h"

#include "slap.h"
#include "config.h"

#include "lutil.h"
#include "ldap_rq.h"

static slap_overinst 		syntax_checks;


static int syntax_checks_do_check( Operation *op, SlapReply *rs )
{
	char		textbuf[ SLAP_TEXT_BUFLEN ];
	size_t		textlen = sizeof( textbuf );
	if (SLAP_DB_DELAY_CHECKS(op->o_bd)) {
		rs->sr_err = slap_mods_final_check( op, op->ora_modlist,
						    textbuf, textlen, NULL );
		if (rs->sr_err != LDAP_SUCCESS) {
			send_ldap_error( op, rs, rs->sr_err, textbuf );
			return rs->sr_err;
		}
	}
	return SLAP_CB_CONTINUE;
}

int syntax_checks_initialize(void)
{
        syntax_checks.on_bi.bi_type = "syntax_checks";
	syntax_checks.on_bi.bi_op_add = syntax_checks_do_check;
	syntax_checks.on_bi.bi_op_modify = syntax_checks_do_check;
	Debug(LDAP_DEBUG_TRACE, ("syntax_checks_initialize\n"),0,0,0);
	return overlay_register(&syntax_checks);
}


#if SLAPD_OVER_SYNTAXCHECKS == SLAPD_MOD_DYNAMIC
int init_module( int argc, char *argv[] )
{
	return syntax_checks_initialize();
}
#endif /* SLAPD_OVER_SYNTAXCHECKS == SLAPD_MOD_DYNAMIC */

#endif /*SLAPD_OVER_SYNTAXCHECKS*/
