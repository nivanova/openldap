
#include "portable.h"

#ifdef SLAPD_OVER_DNSYNTAX

#include <stdio.h>

#include <ac/string.h>
#include <ac/ctype.h>

#include "slap.h"
#include "config.h"
#include "lutil.h"

static slap_overinst dnsyntax;

static int
dnStringSyntaxValidate(
	Syntax		*syntax,
	struct berval	*val )
{
	/* TODO, in samba this is most likely validated in a module, check */
	return LDAP_SUCCESS;
}

static int
dnBinarySyntaxValidate(
	Syntax		*syntax,
	struct berval	*val )
{
	return LDAP_SUCCESS;
}

static char	*dnsyntaxMRs[] = {
	"octetStringMatch",
	NULL
};

static struct {
	char			*oid;
	slap_syntax_defs_rec	syn;
	char			**mrs;
} dnsyntaxes[] = {
	{ "1.2.840.113556.1.4.903" ,
		{ "( 1.2.840.113556.1.4.903 DESC 'DN-String, String + DN' )",
			0,
			NULL,
			dnStringSyntaxValidate,
			NULL },
		dnsyntaxMRs },
	{ "1.2.840.113556.1.4.904" ,
		{ "( 1.2.840.113556.1.4.904 DESC 'DN-Binary, Binary blob + DN' )",
			0,
			NULL,
			dnBinarySyntaxValidate,
			NULL },
		dnsyntaxMRs },
	{ NULL }
};

int dnsyntax_initialize()
{
	int i;

	dnsyntax.on_bi.bi_type = "dnsyntax";

	for ( i=0; dnsyntaxes[i].oid; i++ ) {
		int code;

		code = register_syntax( &dnsyntaxes[ i ].syn );
		if ( code != 0 ) {
			Debug( LDAP_DEBUG_ANY,
				"dnsyntax_init: register_syntax failed\n",
				0, 0, 0 );
			return code;
		}

		if ( dnsyntaxes[i].mrs != NULL ) {
			code = mr_make_syntax_compat_with_mrs(
				dnsyntaxes[i].oid, dnsyntaxes[i].mrs );
			if ( code < 0 ) {
				Debug( LDAP_DEBUG_ANY,
					"dnsyntax_init: "
					"mr_make_syntax_compat_with_mrs "
					"failed\n",
					0, 0, 0 );
				return code;
			}
		}
	}



	return overlay_register(&dnsyntax);
}

#if SLAPD_OVER_DNSYNTAX == SLAPD_MOD_DYNAMIC
int
init_module( int argc, char *argv[] )
{
	return dnsyntax_initialize();
}
#endif

#endif /* SLAPD_OVER_DNSYNTAX */
