/* $OpenLDAP$ */
/* This work is part of OpenLDAP Software <http://www.openldap.org/>.
 *
 * Copyright 1998-2013 The OpenLDAP Foundation.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted only as authorized by the OpenLDAP
 * Public License.
 *
 * A copy of this license is available in the file LICENSE in the
 * top-level directory of the distribution or, alternatively, at
 * <http://www.OpenLDAP.org/license.html>.
 */

/* This file contains flag definitions for the samba-backend overlays */
/* instanceType flags */
#define INSTANCE_TYPE_IS_NC_HEAD	0x00000001
#define INSTANCE_TYPE_UNINSTANT		0x00000002
#define INSTANCE_TYPE_WRITE		0x00000004
#define INSTANCE_TYPE_NC_ABOVE		0x00000008
#define INSTANCE_TYPE_NC_COMING		0x00000010
#define INSTANCE_TYPE_NC_GOING		0x00000020
