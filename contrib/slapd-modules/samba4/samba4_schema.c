/* $OpenLDAP$ */
/* This work is part of OpenLDAP Software <http://www.openldap.org/>.
 *
 * Copyright 1998-2013 The OpenLDAP Foundation.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted only as authorized by the OpenLDAP
 * Public License.
 *
 * A copy of this license is available in the file LICENSE in the
 * top-level directory of the distribution or, alternatively, at
 * <http://www.OpenLDAP.org/license.html>.
 */

/* Loads and supports the samba schema additional features */
#include "portable.h"
#ifdef SLAPD_OVER_SAMBA4SCHEMA
#include <stdio.h>
#include <sys/stat.h>

#include "ac/string.h"
#include "ac/socket.h"

#include "slap.h"
#include "config.h"

#include "lutil.h"
#include "ldap_rq.h"
#include <ldb.h>
#include <talloc.h>
#include "samba_security.h"
#include "core/werror.h"
#include "samba_schema.h"
#include "overlay_utils.h"

static slap_overinst 		samba4_schema;

static ConfigDriver s4s_cf_gen;

enum {
	S4S_INIT_SCHEMA = 1,
	S4S_INIT_PREFIXMAP,
	S4S_SCHEMA_DN,
	S4S_LAST
};



static ConfigTable s4s_cfg[] = {
	{ "samba4-initial-schema", NULL, 2, 2, 0, ARG_MAGIC|S4S_INIT_SCHEMA,
		s4s_cf_gen, "( OLcfgOvAt:22.1 NAME 'olcS4InitialSchemaFile' "
			"DESC 'Path to the file containing the schema metadata for Samba4' "
			"SYNTAX OMsDirectoryString SINGLE-VALUE )", NULL, NULL },
	{ "samba4-initial-prefixmap", NULL, 2, 2, 0, ARG_MAGIC|S4S_INIT_PREFIXMAP,
		s4s_cf_gen, "( OLcfgOvAt:22.2 NAME 'olcS4InitialPrefixMapFile' "
			"DESC 'Path to the file containing the PrefixMap for Samba4' "
			"SYNTAX OMsDirectoryString SINGLE-VALUE )", NULL, NULL },
	{ "samba4-schema-dn", NULL, 2, 2, 0, ARG_MAGIC|S4S_SCHEMA_DN,
		s4s_cf_gen, "( OLcfgOvAt:22.3 NAME 'olcS4SchemaPartitionDn' "
			"DESC 'Path to the file containing the schema metadata for Samba4' "
			"SYNTAX OMsDirectoryString SINGLE-VALUE )", NULL, NULL },
};

static ConfigOCs s4s_ocs[] = {
	{ "( OLcfgOvOc:22.1 "
		"NAME 'olcSamba4SchemaConfig' "
		"DESC 'Samba4Schema module configuration' "
		"SUP olcOverlayConfig "
	        "MUST ( olcS4InitialSchemaFile $ olcS4InitialPrefixMapFile $olcS4SchemaPartitionDn ) )",
			Cft_Overlay, s4s_cfg },
	{ NULL, 0, NULL }
};

static int
s4s_cf_gen(ConfigArgs *c)
{
	slap_overinst *on = (slap_overinst *)c->bi;
	schema4_info *si = (schema4_info *)on->on_bi.bi_private;
	int rc = 0;
	assert(si != NULL);
	if ( c->op == SLAP_CONFIG_EMIT ) {
		switch ( c->type ) {
		case S4S_INIT_SCHEMA:
		{
			struct berval schema_path;
			assert(si->schema_ldif != NULL);
			schema_path.bv_len = strlen(si->schema_ldif);
			schema_path.bv_val = ch_malloc(schema_path.bv_len + 1);
			sprintf(schema_path.bv_val, "%s", si->schema_ldif);
			value_add_one(&c->rvalue_vals, &schema_path);
				/*	assert(si->schema_ldif != NULL);
					c->value_string = ch_strdup(si->schema_ldif); */
			break;
		}
		case S4S_INIT_PREFIXMAP:
		{
			struct berval prefix_path;
			assert(si->prefixmap_ldif != NULL);
			prefix_path.bv_len = strlen(si->prefixmap_ldif);
			prefix_path.bv_val = ch_malloc(prefix_path.bv_len + 1);
			sprintf(prefix_path.bv_val, "%s", si->prefixmap_ldif);
			value_add_one(&c->rvalue_vals, &prefix_path);
			/*assert(si->prefixmap_ldif != NULL);
			  c->value_string = ch_strdup(si->prefixmap_ldif); */
			break;
		}
	       case S4S_SCHEMA_DN:
	       {
		       struct berval schema_dn;
		       assert(si->schema_dn != NULL);
		       schema_dn.bv_len = strlen(si->schema_dn);
		       schema_dn.bv_val = ch_malloc(schema_dn.bv_len + 1);
		       sprintf(schema_dn.bv_val, "%s", si->schema_dn);
		       value_add_one(&c->rvalue_vals, &schema_dn);
		       /*assert(si->schema_dn != NULL);
			 c->value_string = ch_strdup(si->schema_dn); */
		       break;
	       }
		}
		return rc;
	} else if ( c->op == LDAP_MOD_DELETE ) {
		switch ( c->type ) {
		case S4S_INIT_SCHEMA:
			if (si->schema_ldif != NULL) {
				ch_free(si->schema_ldif);
				si->schema_ldif = NULL;
			}
			break;
		case S4S_INIT_PREFIXMAP:
			if (si->prefixmap_ldif != NULL) {
				ch_free(si->prefixmap_ldif);
				si->prefixmap_ldif = NULL;
			}
			break;
	       case S4S_SCHEMA_DN:
		       if (si->schema_dn != NULL) {
				ch_free(si->schema_dn);
				si->schema_dn = NULL;
			}
			break;
		}
		return rc;
	} else {
		switch ( c->type ) {
			case S4S_INIT_SCHEMA:
			if (si->schema_ldif != NULL) {
				ch_free(si->schema_ldif);
			}
			si->schema_ldif = ch_strdup(c->argv[1]);
			break;
		case S4S_INIT_PREFIXMAP:
			if (si->prefixmap_ldif != NULL) {
				ch_free(si->prefixmap_ldif);
			}
			si->prefixmap_ldif = ch_strdup(c->argv[1]);
			break;
	       case S4S_SCHEMA_DN:
		       if (si->schema_dn != NULL) {
				ch_free(si->schema_dn);
			}
		       si->schema_dn = ch_strdup(c->argv[1]);
		       break;
		}
		return rc;
	}
	return rc;
}
static int
samba4_schema_db_init(
	BackendDB *be,
	ConfigReply *cr)
{
	slap_overinst	*on = (slap_overinst *)be->bd_info;
	schema4_info	*si;

	si = ch_calloc(1, sizeof(schema4_info));
	on->on_bi.bi_private = si;
	return 0;
}

static int
samba4_schema_db_open(
	BackendDB *be,
	ConfigReply *cr)
{
	slap_overinst	*on = (slap_overinst *)be->bd_info;
	schema4_info	*si = (schema4_info *)on->on_bi.bi_private;
	si->ldb = ldb_init(NULL, NULL);
	dsdb_set_schema_from_ldif(si->ldb,
				 si->prefixmap_ldif,
				 si->schema_ldif,
				 si->schema_dn,
				 true);
	si->schema = dsdb_get_schema(si->ldb, NULL);
	return 0;
}

static int
samba4_schema_db_destroy(
	BackendDB *be,
	ConfigReply *cr)
{
	slap_overinst	*on = (slap_overinst *)be->bd_info;
	schema4_info	*si = (schema4_info *)on->on_bi.bi_private;

	if (si != NULL) {
		if (si->prefixmap_ldif) {
			ch_free(si->prefixmap_ldif);
		}
		if (si->schema_ldif) {
			ch_free(si->schema_ldif);
		}
		if (si->schema_dn) {
			ch_free(si->schema_dn);
		}
		if (si->ldb) {
			talloc_free(si->ldb);
		}
		ch_free(si);
	}
	return 0;
}


int schema_samba4_initialize(void)
{

	int rc;
	samba4_schema.on_bi.bi_type = "samba4_schema";
	samba4_schema.on_bi.bi_db_init = samba4_schema_db_init;
	samba4_schema.on_bi.bi_db_destroy = samba4_schema_db_destroy;
	samba4_schema.on_bi.bi_db_open = samba4_schema_db_open;

	samba4_schema.on_bi.bi_cf_ocs = s4s_ocs;
	rc = config_register_schema( s4s_cfg, s4s_ocs );
	if ( rc ) {
		return rc;
	}
	return overlay_register(&samba4_schema);
}


#if SLAPD_OVER_SAMBA4SCHEMA == SLAPD_MOD_DYNAMIC
int init_module( int argc, char *argv[] )
{
	return schema_samba4_initialize();
}
#endif /* SLAPD_OVER_SAMBA4SCHEMA == SLAPD_MOD_DYNAMIC */

#endif /*SLAPD_OVER_SAMBA4SCHEMA*/
