 /*
 * Copyright 1998-2013 The OpenLDAP Foundation.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted only as authorized by the OpenLDAP
 * Public License.
 *
 * A copy of this license is available in the file LICENSE in the
 * top-level directory of the distribution or, alternatively, at
 * <http://www.OpenLDAP.org/license.html>.
 */

/* An overlay responsible for the replication meta-data. Functionally equivalent and based on
repl_meta_data samba4 ldb module */

#include "portable.h"

#ifdef SLAPD_OVER_REPLMD

#include <stdio.h>

#include "ac/string.h"
#include "ac/socket.h"

#include "slap.h"
#include "config.h"

#include "lutil.h"
#include "ldap_rq.h"
#include "ldb.h"
#include "samba_security.h"
#include <ndr.h>
#include "overlay_utils.h"

static slap_overinst 		replmd;

static int replmd_op_add( Operation *op, SlapReply *rs )
{
	Attribute *objectguid_attribute = NULL;
	int rc;
	struct GUID guid;
	time_t t = time(NULL);

	Debug(LDAP_DEBUG_ANY, "replmd_op_add\n",0,0,0);
	objectguid_attribute  = ov_find_attribute(op->ora_e->e_attrs, "objectGUID");

	if (objectguid_attribute != NULL) {
		/* todo remove the attributes if they are already provided */
		return SLAP_CB_CONTINUE;
	}

	guid = GUID_random();
    
	rc = ov_add_guid_val(op->ora_e, "objectGUID", &guid);

	if (rc != LDAP_SUCCESS) {
			send_ldap_error( op, rs, rc,
					"Error creating objectGUID." );
			return rs->sr_err;
	}
	
	rc = ov_add_time_val(op->ora_e, "whenChanged", t);
	if ( rc != LDAP_SUCCESS ) {
		send_ldap_error( op, rs, rc,
					"replmd:unable to set attribute 'whenChanged'" );
			return rs->sr_err;
	}

	rc = ov_add_time_val(op->ora_e, "whenCreated", t);
	if ( rc != LDAP_SUCCESS ) {
		send_ldap_error( op, rs, rc,
					"replmd:unable to set attribute 'whenCreated'" );
			return rs->sr_err;
	}

	return SLAP_CB_CONTINUE;
}

static int replmd_op_modify( Operation *op, SlapReply *rs )
{
	time_t t = time(NULL);
	Modification* md = ov_find_modification(op->orm_modlist, "objectGUID");
	Modifications *ml, *mod;
	AttributeDescription *ad_whenChanged = NULL;
	struct berval val;
	int rc;

	if ( (op->o_tag == LDAP_REQ_MODIFY) && (md != NULL)) {
		send_ldap_error( op, rs, LDAP_CONSTRAINT_VIOLATION,
					"replmd: objectGUID must not be specified!" );
			return rs->sr_err;
	}

	ad_whenChanged = ov_find_attr_description("whenChanged");
	if (ad_whenChanged == NULL) {
		send_ldap_error( op, rs, LDAP_NO_SUCH_ATTRIBUTE,
					"replmd: cannot find whenChanged in schema!" );
			return rs->sr_err;
	}
	if (rc = ov_timestring(&val, t) != LDAP_SUCCESS) {
		send_ldap_error( op, rs, LDAP_OTHER,
					"replmd: cannot create whenChanged value!" );
			return rs->sr_err;
	}
	mod = ch_calloc( sizeof( Modifications ), 1 );	
	for ( ml = op->orm_modlist; ml && ml->sml_next; ml = ml->sml_next );
	ml->sml_next = mod;
	mod->sml_desc = ad_whenChanged;
	mod->sml_numvals = 1;
	value_add_one( &mod->sml_values, &val );
	mod->sml_nvalues = NULL;
	mod->sml_op = LDAP_MOD_REPLACE;
	mod->sml_flags = 0;
	mod->sml_next = NULL;
	return SLAP_CB_CONTINUE;
}

int replmd_initialize(void)
{
	replmd.on_bi.bi_type = "replmd";
	replmd.on_bi.bi_op_add = replmd_op_add;
	replmd.on_bi.bi_op_modify = replmd_op_modify;
	replmd.on_bi.bi_op_modrdn = replmd_op_modify;
	Debug(LDAP_DEBUG_TRACE, "replmd_initialize\n",0,0,0);
	return overlay_register(&replmd);
}


#if SLAPD_OVER_REPLMD == SLAPD_MOD_DYNAMIC
int init_module( int argc, char *argv[] )
{
	return replmd_initialize();
}
#endif /* SLAPD_OVER_REPLMD == SLAPD_MOD_DYNAMIC */

#endif /*SLAPD_OVER_REPLMD*/
