/* $OpenLDAP$ */
/* This work is part of OpenLDAP Software <http://www.openldap.org/>.
 *
 * Copyright 1998-2013 The OpenLDAP Foundation.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted only as authorized by the OpenLDAP
 * Public License.
 *
 * A copy of this license is available in the file LICENSE in the
 * top-level directory of the distribution or, alternatively, at
 * <http://www.OpenLDAP.org/license.html>.
 */

#ifndef OVERLAY_UTILS_H
#define OVERLAY_UTILS_H

#include "portable.h"
#include "slap.h"
#include "config.h"
#include "samba_security.h"
#include "samba_schema.h"
#include <ldb.h>

typedef struct schema4_info {
	struct ldb_context *ldb;
	struct dsdb_schema *schema;
	char *prefixmap_ldif;
	char *schema_ldif;
	char *schema_dn;
} schema4_info;

typedef struct opprep_info_add {
	struct berval parent_sd;
	struct berval object_guid;
}opprep_info_add;

typedef struct opprep_info_search {
	struct berval parent_sd;
	struct berval object_guid;
}opprep_info_search;

typedef struct opprep_info_mod {
	struct berval object_class;
	struct berval instanceType;
	struct berval sd;
}opprep_info_mod;

typedef struct opprep_info_modrdn {
	struct berval parent_sd;
	struct berval new_parent_sd;
}opprep_info_modrdn;

typedef struct opprep_info_del {
	struct berval parent_sd;
}opprep_info_del;

typedef union opprep_un {
	opprep_info_add    add;
	opprep_info_mod    mod;
	opprep_info_modrdn modrdn;
	opprep_info_del    del;
	opprep_info_search search;
}opprep_un;

typedef struct opprep_info {
	struct dsdb_schema *schema;
	uint32_t internal_flags;
	uint32_t sd_flags;
	DATA_BLOB *sec_token;
	bool is_trusted;
	opprep_un un; 
} opprep_info_t;

typedef struct OpExtraOpprep {
	OpExtra oe;
	opprep_info_t *oe_opi;
} OpExtraOpprep;

void attr_delvals( Attribute *a );
int set_partitions_db_pointers( BackendDB *be );
SD_PARTITION get_partition_flag(Operation *op);

BackendDB *get_schema_db();
BackendDB *get_domain_db();
BackendDB *get_config_db();

struct dsdb_schema *get_samba_schema();
bool dn_has_extended(struct berval *dn);
int dn_remove_extended( struct berval *dn, struct berval *dst, void *ctx );

int ov_execute_search(Operation *op,
		      SlapReply *rs,
		      BackendDB *backend_db,
		      struct berval *base,
		      char **attr_list,
		      int attr_count,
		      char *filter,
		      int scope,
		      slap_callback *cb,
		      LDAPControl **ctrls,
		      bool skip_ol_stack);

Attribute* ov_find_attribute(Attribute *attr_list, const char *attr_name);
Modification* ov_find_modification(Modifications *mod_list, const char *attr_name);
int ov_find_attribute_int(Attribute *attr_list, const char *attr_name, int default_val, int index);
int ov_timestring(struct berval *val, time_t t);
int ov_add_guid_val(Entry *e, char *name, struct GUID *guid);
int ov_add_uint64_val(Entry *e, char *name, uint64_t value);
int ov_add_int64_val(Entry *e, char *name, int64_t value);
int ov_add_time_val(Entry *e, char *name, time_t value);

AttributeDescription* ov_find_attr_description(const char *attr_name);

bool ov_is_trusted_connection(Operation *op);

/* This is just to identify the op_prep module... todo find a neater way */
const char *opprep_id = "Opprep extra";
/* custom control containing the security token */
#define LDAP_CONTROL_SECDESC_TOKEN_OID "1.3.6.1.4.1.7165.4.3.22"
#endif /*OVERLAY_UTILS_H*/
