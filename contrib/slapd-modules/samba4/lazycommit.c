
/* $OpenLDAP$ */
/* This work is part of OpenLDAP Software <http://www.openldap.org/>.
 *
 * Copyright 1998-2013 The OpenLDAP Foundation.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted only as authorized by the OpenLDAP
 * Public License.
 *
 * A copy of this license is available in the file LICENSE in the
 * top-level directory of the distribution or, alternatively, at
 * <http://www.OpenLDAP.org/license.html>.
 */

/* just ignore the lazy commit */

#include "portable.h"

#ifdef SLAPD_OVER_LAZYCOMMIT

#include <stdio.h>

#include "ac/string.h"
#include "ac/socket.h"

#include "slap.h"
#include "config.h"

#include "lutil.h"
#include "ldap_rq.h"
#include "ldb.h"

static slap_overinst 		lazycommit;
static int lazycommit_cid;
#define o_lazycommit			o_ctrlflag[lazycommit_cid]
#define o_ctrl_lazycommit		o_controls[lazycommit_cid]

static int
lazycommit_parseCtrl(
	Operation *op,
	SlapReply *rs,
	LDAPControl *ctrl )
{
	op->o_lazycommit = SLAP_CONTROL_NONCRITICAL;
	ctrl->ldctl_iscritical = 0;
	return LDAP_SUCCESS;
}


int lazycommit_initialize(void)
{
	int rc;
	rc = register_supported_control(LDB_CONTROL_SERVER_LAZY_COMMIT,
					 SLAP_CTRL_SEARCH|SLAP_CTRL_ADD|SLAP_CTRL_DELETE|SLAP_CTRL_MODIFY|SLAP_CTRL_RENAME,
					 NULL,
					 lazycommit_parseCtrl, &lazycommit_cid );
	if ( rc != LDAP_SUCCESS ) {
		Debug( LDAP_DEBUG_ANY,
			"lazycommit_initialize: Failed to register control (%d)\n",
			rc, 0, 0 );
		return -1;
		}


	lazycommit.on_bi.bi_type = "lazycommit";
	Debug(LDAP_DEBUG_TRACE, "lazycommit_initialize\n",0,0,0);
	return overlay_register(&lazycommit);
}


#if SLAPD_OVER_LAZYCOMMIT == SLAPD_MOD_DYNAMIC
int init_module( int argc, char *argv[] )
{
	return lazycommit_initialize();
}
#endif /* SLAPD_OVER_LAZYCOMMIT == SLAPD_MOD_DYNAMIC */

#endif /*SLAPD_OVER_LAZYCOMMIT*/
